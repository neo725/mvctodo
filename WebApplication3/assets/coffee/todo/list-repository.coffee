module.exports = ['api', '$q', (api, $q) ->
    deferedTodos = $q.defer()

    repository = {
        initTodos: (before_works) ->
            deferedTodos = $q.defer()
            before_works()
            doInitTodos()
            deferedTodos.promise

        addTodo: (data, before_works) ->
            deferedTodos = $q.defer()
            before_works()
            doAddTodo(data)
            deferedTodos.promise

        deleteTodo: (data, before_works) ->
            deferedTodos = $q.defer()
            before_works()
            doDeleteTodo(data)
            deferedTodos.promise

        deleteTodos: (datas, before_works) ->
            deferedTodos = $q.defer()
            before_works()
            doDeleteTodos(datas)
            deferedTodos.promise

        updateTodo: (data, before_work) ->
            deferedTodos = $q.defer()
            before_work()
            doUpdateTodo(data)
            deferedTodos.promise
    }

    doInitTodos = () ->
        onSuccess = (data) ->
            deferedTodos.resolve(data.value)
        api.getTodos(onSuccess, (->))

    doAddTodo = (data) ->
        onSuccess = (response) ->
            deferedTodos.resolve(response)
        onError = (response, status) ->
            deferedTodos.reject({response, status})
        api.addTodo(data, onSuccess, onError)

    doDeleteTodo = (data) ->
        onSuccess = (response) ->
            deferedTodos.resolve(response)
        onError = (response, status) ->
            deferedTodos.reject({response, status})
        api.deleteTodo(data, onSuccess, onError)

    doDeleteTodos = (datas) ->
        onSuccess = (response) ->
            deferedTodos.resolve(response)
        onError = (response, status) ->
            deferedTodos.reject({response, status})
        api.deleteTodos(datas, onSuccess, onError)

    doUpdateTodo = (data) ->
        onSuccess = (response) ->
            deferedTodos.resolve(response)
        onError = (response, status) ->
            deferedTodos.reject({response, status})
        api.updateTodo(data, onSuccess, onError)

    return repository
]