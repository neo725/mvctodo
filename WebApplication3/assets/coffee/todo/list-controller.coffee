_ = require('lodash')
filters = require('../common/filters')

module.exports = ['$rootScope', '$scope', '$q', 'repository',
    ($rootScope, $scope, $q, repository) ->
        lastId = 0
        $scope.todos = []
        $scope.errors = {}

        makeTodo = (id, title, created, updated, selected) ->
            selected = false if selected == undefined
            item =
                id: id,
                title: title,
                created: created,
                updated: updated,
                isedit: false,
                selected: selected,

            lastId = id + 1
            item

        initTodo = () ->
            repository.initTodos(()->
                $rootScope.isloading = true
            ).then (todos)->
                $scope.todos = []
                _.each todos, (todo)->
                    $scope.todos.push makeTodo todo.TodoId, todo.Title, todo.Created, todo.Updated, todo.Checked
                $rootScope.isloading = false

        $scope.doEditMode = (item) ->
            item.isedit = !item.isedit
            item.copy_title = angular.copy(item.title)

        $scope.doUpdateEdit = (item) ->
            if (item.title == item.copy_title)
                return

            data =
                TodoId: item.id
                Title: item.copy_title

            before_work = () ->
                $rootScope.isloading = true

            repository.updateTodo(data, before_work).then (response) ->
#                item = makeTodo response.TodoId, response.Title, response.Created, response.Updated, response.Checked
                item.isedit = false
                item.title = response.Title
                item.updated = response.Updated

                $rootScope.isloading = false
            , (reason) ->
                status = reason.status
                if status == 404
                    console.log 'Resource not found.'
                item.isedit = false
                $rootScope.isloading = false

        $scope.doDelete = (item) ->
            index = $scope.todos.indexOf(item)

            before_work = () ->
                $rootScope.isloading = true

            repository.deleteTodo(item, before_work).then (response) ->
                $scope.todos.splice(index, 1)
                $rootScope.isloading = false
            , (reason) ->
                status = reason.status
                if status == 404
                    console.log 'Resource not found'
                $rootScope.isloading = false

        $scope.doAdd = () ->
            $rootScope.isloading = true
#            now = new Date().getTime()
            data =
#                Title: $scope.newtitle,
#                Created: filters.utcdatetime(now)
                Title: $scope.newtitle

            before_work = (->)

            repository.addTodo(data, before_work).then (response) ->
                $scope.todos.unshift makeTodo response.TodoId, response.Title, response.Created, response.Updated,
                    response.Checked
                $rootScope.isloading = false
            , (reason) ->
                response = reason.response
                status = reason.status
                if status == 400
                    errors = JSON.parse(response['odata.error'].message.value)
                    $scope.errors = errors
                $rootScope.isloading = false
            $scope.newtitle = ""

        $scope.doDeleteCheckItems = (todos) ->
            items = _.filter(todos, {'selected': true})
            before_work = () ->
                $rootScope.isloading = true

            repository.deleteTodos(items, before_work).then (response) ->
                $rootScope.isloading = false
                initTodo()
            , (reason) ->
                status = reason.status
                $rootScope.isloading = false

        return initTodo()
]