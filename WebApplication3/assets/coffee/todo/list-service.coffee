module.exports = ['$http', ($http) ->
    api = {
        getTodos: (onSuccess, onError) ->
            url = '/odata/Todos'
            $http.get(url)
                .success(onSuccess)
                .error(onError)

        addTodo: (data, onSuccess, onError) ->
            url = '/odata/Todos'
            $http.post(url, data)
                .success(onSuccess)
                .error(onError)

        deleteTodo: (data, onSuccess, onError) ->
            url = '/odata/Todos'
            $http.delete(url)
                .success(onSuccess)
                .error(onError)

        deleteTodos: (datas, onSuccess, onError) ->
            keys = _.map datas, 'id'
            url = "/odata/Todos/Delete"

            $http.post(url, {keys: keys})
                .success(onSuccess)
                .error(onError)

        updateTodo: (data, onSuccess, onError) ->
            data_id = data.TodoId
            url = "/odata/Todos(#{data_id})"
            $http.put(url, data)
                .success(onSuccess)
                .error(onError)
    }

    return api
]