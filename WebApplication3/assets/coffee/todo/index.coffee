require('../common')

angular.module("todo", ['common'])
    .service('api', require('./list-service'))
    .factory('repository', require('./list-repository'))
    .controller("ListController", require("./list-controller"))