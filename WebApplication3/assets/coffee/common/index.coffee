directives = require('./directives')

angular.module('common', [])
    .directive('serverValidated', directives.serverValidated)
    .directive('errorFor', directives.errorFor)
    .directive('confirmDialog', directives.confirmDialog)
    .directive('hasChecked', directives.hasChecked)