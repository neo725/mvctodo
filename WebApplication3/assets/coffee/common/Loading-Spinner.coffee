module.exports = (scope) ->
    show: ->
        scope.loading = true

    hide: ->
        scope.loading = false
