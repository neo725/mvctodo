utils = require('./utils')

exports.serverValidated = ->
    restrict: 'A'
    require: '?ngModel'
    link: (scope, element, attrs, ctrl) ->
        if element.prop('tagName') == 'FORM'
            scope.$watch "errors", (errors) ->
                utils.populateFormError(element, errors, element.attr('name'))
        else if element.prop('tagName') == 'DIV'
            element.on 'click dragenter', ->
                scope.$apply ->
                    ctrl.$setValidity('server', true)
        else
            scope.$watch(
                ->
                    ctrl.$modelValue
            ,
                ->
                    ctrl.$setValidity('server', true)
                    scope.$apply() if not scope.$$phase
            )

exports.errorFor = ->
    require: '^form'
    template: '{{message}}'
    scope: {}
    link: (scope, elem, attrs, ctrl) ->
        formName = ctrl.$name
        fieldName = attrs.errorFor
        elem.addClass('error')

        updateErrorMessage = () ->
            field = scope.$parent[formName][fieldName]

            if field && field.$invalid && field.error
                elem.show()
                scope.message = field.error
            else
                elem.hide()
                scope.message = ''

        scope.$watch "$parent.#{formName}.#{fieldName}.$error.server", updateErrorMessage

exports.confirmDialog = ->
    priority: -1,
    restrict: 'A',
    link:
        pre: (scope, element, attr) ->
            msg = attr.confirmDialog or 'Are you sure?'
            element.bind 'click', (e) ->
                if not window.confirm msg
                    e.stopImmediatePropagation()
                    e.preventDefault()

exports.hasChecked = ->
    priority: 2,
    restrict: 'A',
    link:
        pre: (scope, element, attr) ->
            element.bind 'click', (e) ->
                items = scope.$eval(attr.hasChecked)
                count = 0
                _.each items, (item) ->
                    count += 1 if item[attr.hasCheckedProp]
                console.log count
                if (count == 0)
                    alert attr.hasCheckedPrompt
                    e.stopImmediatePropagation()
                    e.preventDefault()