exports.utcdatetime = (date) ->
    m = moment.utc(date)
    return if m.isValid() then m.format('YYYY-MM-DDTHH:mm:ss') else date