module.exports =
    populateFormError: (formElement, errors, formName = 'form', externalForm) ->
        resetFormControlError = ->
            _.forOwn(form, (prop) ->
                if prop and prop.$name
                    if (not _.has(errors, prop.$name)) and _.has(form[prop.$name], 'error')
                        delete form[prop.$name].error
                        form[prop.$name].$setValidity('server', true)
            )

        externalForm = externalForm or formElement
        form = formElement.scope()[formName]
        resetFormControlError()

        for fieldName, fieldErrors of errors
            continue if fieldErrors.length == 0

            if angular.isObject(fieldErrors[0])
                subformElements = angular.element("[ng-form='#{fieldName}']", formElement)
                for subformElement, index in subformElements
                    this.populateFormError(angular.element(subformElement), fieldErrors[index], fieldName, externalForm)
            else
                continue if not form[fieldName]
                form[fieldName].$setValidity('server', false)
                form[fieldName].error = fieldErrors[0]

        return if externalForm != formElement

        invalidFormElements = $(":not(form,ngForm,[ng-form]).ng-invalid", formElement)
        if invalidFormElements.length > 0
            invalidFormElements[0].focus()