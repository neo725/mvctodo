var _, filters;

_ = require('lodash');

filters = require('../common/filters');

module.exports = [
  '$rootScope', '$scope', '$q', 'repository', function($rootScope, $scope, $q, repository) {
    var initTodo, lastId, makeTodo;
    lastId = 0;
    $scope.todos = [];
    $scope.errors = {};
    makeTodo = function(id, title, created, updated, selected) {
      var item;
      if (selected === void 0) {
        selected = false;
      }
      item = {
        id: id,
        title: title,
        created: created,
        updated: updated,
        isedit: false,
        selected: selected
      };
      lastId = id + 1;
      return item;
    };
    initTodo = function() {
      return repository.initTodos(function() {
        return $rootScope.isloading = true;
      }).then(function(todos) {
        $scope.todos = [];
        _.each(todos, function(todo) {
          return $scope.todos.push(makeTodo(todo.TodoId, todo.Title, todo.Created, todo.Updated, todo.Checked));
        });
        return $rootScope.isloading = false;
      });
    };
    $scope.doEditMode = function(item) {
      item.isedit = !item.isedit;
      return item.copy_title = angular.copy(item.title);
    };
    $scope.doUpdateEdit = function(item) {
      var before_work, data;
      if (item.title === item.copy_title) {
        return;
      }
      data = {
        TodoId: item.id,
        Title: item.copy_title
      };
      before_work = function() {
        return $rootScope.isloading = true;
      };
      return repository.updateTodo(data, before_work).then(function(response) {
        item.isedit = false;
        item.title = response.Title;
        item.updated = response.Updated;
        return $rootScope.isloading = false;
      }, function(reason) {
        var status;
        status = reason.status;
        if (status === 404) {
          console.log('Resource not found.');
        }
        item.isedit = false;
        return $rootScope.isloading = false;
      });
    };
    $scope.doDelete = function(item) {
      var before_work, index;
      index = $scope.todos.indexOf(item);
      before_work = function() {
        return $rootScope.isloading = true;
      };
      return repository.deleteTodo(item, before_work).then(function(response) {
        $scope.todos.splice(index, 1);
        return $rootScope.isloading = false;
      }, function(reason) {
        var status;
        status = reason.status;
        if (status === 404) {
          console.log('Resource not found');
        }
        return $rootScope.isloading = false;
      });
    };
    $scope.doAdd = function() {
      var before_work, data;
      $rootScope.isloading = true;
      data = {
        Title: $scope.newtitle
      };
      before_work = (function() {});
      repository.addTodo(data, before_work).then(function(response) {
        $scope.todos.unshift(makeTodo(response.TodoId, response.Title, response.Created, response.Updated, response.Checked));
        return $rootScope.isloading = false;
      }, function(reason) {
        var errors, response, status;
        response = reason.response;
        status = reason.status;
        if (status === 400) {
          errors = JSON.parse(response['odata.error'].message.value);
          $scope.errors = errors;
        }
        return $rootScope.isloading = false;
      });
      return $scope.newtitle = "";
    };
    $scope.doDeleteCheckItems = function(todos) {
      var before_work, items;
      items = _.filter(todos, {
        'selected': true
      });
      before_work = function() {
        return $rootScope.isloading = true;
      };
      return repository.deleteTodos(items, before_work).then(function(response) {
        $rootScope.isloading = false;
        return initTodo();
      }, function(reason) {
        var status;
        status = reason.status;
        return $rootScope.isloading = false;
      });
    };
    return initTodo();
  }
];
