module.exports = [
  '$http', function($http) {
    var api;
    api = {
      getTodos: function(onSuccess, onError) {
        var url;
        url = '/odata/Todos';
        return $http.get(url).success(onSuccess).error(onError);
      },
      addTodo: function(data, onSuccess, onError) {
        var url;
        url = '/odata/Todos';
        return $http.post(url, data).success(onSuccess).error(onError);
      },
      deleteTodo: function(data, onSuccess, onError) {
        var url;
        url = '/odata/Todos';
        return $http["delete"](url).success(onSuccess).error(onError);
      },
      deleteTodos: function(datas, onSuccess, onError) {
        var keys, url;
        keys = _.map(datas, 'id');
        url = "/odata/Todos/Delete";
        return $http.post(url, {
          keys: keys
        }).success(onSuccess).error(onError);
      },
      updateTodo: function(data, onSuccess, onError) {
        var data_id, url;
        data_id = data.TodoId;
        url = "/odata/Todos(" + data_id + ")";
        return $http.put(url, data).success(onSuccess).error(onError);
      }
    };
    return api;
  }
];
