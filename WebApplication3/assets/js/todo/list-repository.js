module.exports = [
  'api', '$q', function(api, $q) {
    var deferedTodos, doAddTodo, doDeleteTodo, doDeleteTodos, doInitTodos, doUpdateTodo, repository;
    deferedTodos = $q.defer();
    repository = {
      initTodos: function(before_works) {
        deferedTodos = $q.defer();
        before_works();
        doInitTodos();
        return deferedTodos.promise;
      },
      addTodo: function(data, before_works) {
        deferedTodos = $q.defer();
        before_works();
        doAddTodo(data);
        return deferedTodos.promise;
      },
      deleteTodo: function(data, before_works) {
        deferedTodos = $q.defer();
        before_works();
        doDeleteTodo(data);
        return deferedTodos.promise;
      },
      deleteTodos: function(datas, before_works) {
        deferedTodos = $q.defer();
        before_works();
        doDeleteTodos(datas);
        return deferedTodos.promise;
      },
      updateTodo: function(data, before_work) {
        deferedTodos = $q.defer();
        before_work();
        doUpdateTodo(data);
        return deferedTodos.promise;
      }
    };
    doInitTodos = function() {
      var onSuccess;
      onSuccess = function(data) {
        return deferedTodos.resolve(data.value);
      };
      return api.getTodos(onSuccess, (function() {}));
    };
    doAddTodo = function(data) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.addTodo(data, onSuccess, onError);
    };
    doDeleteTodo = function(data) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.deleteTodo(data, onSuccess, onError);
    };
    doDeleteTodos = function(datas) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.deleteTodos(datas, onSuccess, onError);
    };
    doUpdateTodo = function(data) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.updateTodo(data, onSuccess, onError);
    };
    return repository;
  }
];
