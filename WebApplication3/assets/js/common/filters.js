exports.utcdatetime = function(date) {
  var m;
  m = moment.utc(date);
  if (m.isValid()) {
    return m.format('YYYY-MM-DDTHH:mm:ss');
  } else {
    return date;
  }
};
