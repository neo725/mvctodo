module.exports = function(scope) {
  return {
    show: function() {
      return scope.loading = true;
    },
    hide: function() {
      return scope.loading = false;
    }
  };
};
