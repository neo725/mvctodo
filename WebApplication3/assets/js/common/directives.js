var utils;

utils = require('./utils');

exports.serverValidated = function() {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function(scope, element, attrs, ctrl) {
      if (element.prop('tagName') === 'FORM') {
        return scope.$watch("errors", function(errors) {
          return utils.populateFormError(element, errors, element.attr('name'));
        });
      } else if (element.prop('tagName') === 'DIV') {
        return element.on('click dragenter', function() {
          return scope.$apply(function() {
            return ctrl.$setValidity('server', true);
          });
        });
      } else {
        return scope.$watch(function() {
          return ctrl.$modelValue;
        }, function() {
          ctrl.$setValidity('server', true);
          if (!scope.$$phase) {
            return scope.$apply();
          }
        });
      }
    }
  };
};

exports.errorFor = function() {
  return {
    require: '^form',
    template: '{{message}}',
    scope: {},
    link: function(scope, elem, attrs, ctrl) {
      var fieldName, formName, updateErrorMessage;
      formName = ctrl.$name;
      fieldName = attrs.errorFor;
      elem.addClass('error');
      updateErrorMessage = function() {
        var field;
        field = scope.$parent[formName][fieldName];
        if (field && field.$invalid && field.error) {
          elem.show();
          return scope.message = field.error;
        } else {
          elem.hide();
          return scope.message = '';
        }
      };
      return scope.$watch("$parent." + formName + "." + fieldName + ".$error.server", updateErrorMessage);
    }
  };
};

exports.confirmDialog = function() {
  return {
    priority: -1,
    restrict: 'A',
    link: {
      pre: function(scope, element, attr) {
        var msg;
        msg = attr.confirmDialog || 'Are you sure?';
        return element.bind('click', function(e) {
          if (!window.confirm(msg)) {
            e.stopImmediatePropagation();
            return e.preventDefault();
          }
        });
      }
    }
  };
};

exports.hasChecked = function() {
  return {
    priority: 2,
    restrict: 'A',
    link: {
      pre: function(scope, element, attr) {
        return element.bind('click', function(e) {
          var count, items;
          items = scope.$eval(attr.hasChecked);
          count = 0;
          _.each(items, function(item) {
            if (item[attr.hasCheckedProp]) {
              return count += 1;
            }
          });
          console.log(count);
          if (count === 0) {
            alert(attr.hasCheckedPrompt);
            e.stopImmediatePropagation();
            return e.preventDefault();
          }
        });
      }
    }
  };
};
