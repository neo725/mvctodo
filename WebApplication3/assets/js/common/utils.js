module.exports = {
  populateFormError: function(formElement, errors, formName, externalForm) {
    var fieldErrors, fieldName, form, i, index, invalidFormElements, len, resetFormControlError, subformElement, subformElements;
    if (formName == null) {
      formName = 'form';
    }
    resetFormControlError = function() {
      return _.forOwn(form, function(prop) {
        if (prop && prop.$name) {
          if ((!_.has(errors, prop.$name)) && _.has(form[prop.$name], 'error')) {
            delete form[prop.$name].error;
            return form[prop.$name].$setValidity('server', true);
          }
        }
      });
    };
    externalForm = externalForm || formElement;
    form = formElement.scope()[formName];
    resetFormControlError();
    for (fieldName in errors) {
      fieldErrors = errors[fieldName];
      if (fieldErrors.length === 0) {
        continue;
      }
      if (angular.isObject(fieldErrors[0])) {
        subformElements = angular.element("[ng-form='" + fieldName + "']", formElement);
        for (index = i = 0, len = subformElements.length; i < len; index = ++i) {
          subformElement = subformElements[index];
          this.populateFormError(angular.element(subformElement), fieldErrors[index], fieldName, externalForm);
        }
      } else {
        if (!form[fieldName]) {
          continue;
        }
        form[fieldName].$setValidity('server', false);
        form[fieldName].error = fieldErrors[0];
      }
    }
    if (externalForm !== formElement) {
      return;
    }
    invalidFormElements = $(":not(form,ngForm,[ng-form]).ng-invalid", formElement);
    if (invalidFormElements.length > 0) {
      return invalidFormElements[0].focus();
    }
  }
};
