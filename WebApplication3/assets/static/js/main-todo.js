(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var utils;

utils = require('./utils');

exports.serverValidated = function() {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function(scope, element, attrs, ctrl) {
      if (element.prop('tagName') === 'FORM') {
        return scope.$watch("errors", function(errors) {
          return utils.populateFormError(element, errors, element.attr('name'));
        });
      } else if (element.prop('tagName') === 'DIV') {
        return element.on('click dragenter', function() {
          return scope.$apply(function() {
            return ctrl.$setValidity('server', true);
          });
        });
      } else {
        return scope.$watch(function() {
          return ctrl.$modelValue;
        }, function() {
          ctrl.$setValidity('server', true);
          if (!scope.$$phase) {
            return scope.$apply();
          }
        });
      }
    }
  };
};

exports.errorFor = function() {
  return {
    require: '^form',
    template: '{{message}}',
    scope: {},
    link: function(scope, elem, attrs, ctrl) {
      var fieldName, formName, updateErrorMessage;
      formName = ctrl.$name;
      fieldName = attrs.errorFor;
      elem.addClass('error');
      updateErrorMessage = function() {
        var field;
        field = scope.$parent[formName][fieldName];
        if (field && field.$invalid && field.error) {
          elem.show();
          return scope.message = field.error;
        } else {
          elem.hide();
          return scope.message = '';
        }
      };
      return scope.$watch("$parent." + formName + "." + fieldName + ".$error.server", updateErrorMessage);
    }
  };
};

exports.confirmDialog = function() {
  return {
    priority: -1,
    restrict: 'A',
    link: {
      pre: function(scope, element, attr) {
        var msg;
        msg = attr.confirmDialog || 'Are you sure?';
        return element.bind('click', function(e) {
          if (!window.confirm(msg)) {
            e.stopImmediatePropagation();
            return e.preventDefault();
          }
        });
      }
    }
  };
};

exports.hasChecked = function() {
  return {
    priority: 2,
    restrict: 'A',
    link: {
      pre: function(scope, element, attr) {
        return element.bind('click', function(e) {
          var count, items;
          items = scope.$eval(attr.hasChecked);
          count = 0;
          _.each(items, function(item) {
            if (item[attr.hasCheckedProp]) {
              return count += 1;
            }
          });
          console.log(count);
          if (count === 0) {
            alert(attr.hasCheckedPrompt);
            e.stopImmediatePropagation();
            return e.preventDefault();
          }
        });
      }
    }
  };
};

},{"./utils":4}],2:[function(require,module,exports){
exports.utcdatetime = function(date) {
  var m;
  m = moment.utc(date);
  if (m.isValid()) {
    return m.format('YYYY-MM-DDTHH:mm:ss');
  } else {
    return date;
  }
};

},{}],3:[function(require,module,exports){
var directives;

directives = require('./directives');

angular.module('common', [])
    .directive('serverValidated', directives.serverValidated)
    .directive('errorFor', directives.errorFor)
    .directive('confirmDialog', directives.confirmDialog)
    .directive('hasChecked', directives.hasChecked);

},{"./directives":1}],4:[function(require,module,exports){
module.exports = {
  populateFormError: function(formElement, errors, formName, externalForm) {
    var fieldErrors, fieldName, form, i, index, invalidFormElements, len, resetFormControlError, subformElement, subformElements;
    if (formName == null) {
      formName = 'form';
    }
    resetFormControlError = function() {
      return _.forOwn(form, function(prop) {
        if (prop && prop.$name) {
          if ((!_.has(errors, prop.$name)) && _.has(form[prop.$name], 'error')) {
            delete form[prop.$name].error;
            return form[prop.$name].$setValidity('server', true);
          }
        }
      });
    };
    externalForm = externalForm || formElement;
    form = formElement.scope()[formName];
    resetFormControlError();
    for (fieldName in errors) {
      fieldErrors = errors[fieldName];
      if (fieldErrors.length === 0) {
        continue;
      }
      if (angular.isObject(fieldErrors[0])) {
        subformElements = angular.element("[ng-form='" + fieldName + "']", formElement);
        for (index = i = 0, len = subformElements.length; i < len; index = ++i) {
          subformElement = subformElements[index];
          this.populateFormError(angular.element(subformElement), fieldErrors[index], fieldName, externalForm);
        }
      } else {
        if (!form[fieldName]) {
          continue;
        }
        form[fieldName].$setValidity('server', false);
        form[fieldName].error = fieldErrors[0];
      }
    }
    if (externalForm !== formElement) {
      return;
    }
    invalidFormElements = $(":not(form,ngForm,[ng-form]).ng-invalid", formElement);
    if (invalidFormElements.length > 0) {
      return invalidFormElements[0].focus();
    }
  }
};

},{}],5:[function(require,module,exports){
require("./todo");

},{"./todo":6}],6:[function(require,module,exports){
require('../common');

angular.module("todo", ['common'])
    .service('api', require('./list-service'))
    .factory('repository', require('./list-repository'))
    .controller("ListController", require("./list-controller"));

},{"../common":3,"./list-controller":7,"./list-repository":8,"./list-service":9}],7:[function(require,module,exports){
var _, filters;

_ = require('lodash');

filters = require('../common/filters');

module.exports = [
  '$rootScope', '$scope', '$q', 'repository', function($rootScope, $scope, $q, repository) {
    var initTodo, lastId, makeTodo;
    lastId = 0;
    $scope.todos = [];
    $scope.errors = {};
    makeTodo = function(id, title, created, updated, selected) {
      var item;
      if (selected === void 0) {
        selected = false;
      }
      item = {
        id: id,
        title: title,
        created: created,
        updated: updated,
        isedit: false,
        selected: selected
      };
      lastId = id + 1;
      return item;
    };
    initTodo = function() {
      return repository.initTodos(function() {
        return $rootScope.isloading = true;
      }).then(function(todos) {
        $scope.todos = [];
        _.each(todos, function(todo) {
          return $scope.todos.push(makeTodo(todo.TodoId, todo.Title, todo.Created, todo.Updated, todo.Checked));
        });
        return $rootScope.isloading = false;
      });
    };
    $scope.doEditMode = function(item) {
      item.isedit = !item.isedit;
      return item.copy_title = angular.copy(item.title);
    };
    $scope.doUpdateEdit = function(item) {
      var before_work, data;
      if (item.title === item.copy_title) {
        return;
      }
      data = {
        TodoId: item.id,
        Title: item.copy_title
      };
      before_work = function() {
        return $rootScope.isloading = true;
      };
      return repository.updateTodo(data, before_work).then(function(response) {
        item.isedit = false;
        item.title = response.Title;
        item.updated = response.Updated;
        return $rootScope.isloading = false;
      }, function(reason) {
        var status;
        status = reason.status;
        if (status === 404) {
          console.log('Resource not found.');
        }
        item.isedit = false;
        return $rootScope.isloading = false;
      });
    };
    $scope.doDelete = function(item) {
      var before_work, index;
      index = $scope.todos.indexOf(item);
      before_work = function() {
        return $rootScope.isloading = true;
      };
      return repository.deleteTodo(item, before_work).then(function(response) {
        $scope.todos.splice(index, 1);
        return $rootScope.isloading = false;
      }, function(reason) {
        var status;
        status = reason.status;
        if (status === 404) {
          console.log('Resource not found');
        }
        return $rootScope.isloading = false;
      });
    };
    $scope.doAdd = function() {
      var before_work, data;
      $rootScope.isloading = true;
      data = {
        Title: $scope.newtitle
      };
      before_work = (function() {});
      repository.addTodo(data, before_work).then(function(response) {
        $scope.todos.unshift(makeTodo(response.TodoId, response.Title, response.Created, response.Updated, response.Checked));
        return $rootScope.isloading = false;
      }, function(reason) {
        var errors, response, status;
        response = reason.response;
        status = reason.status;
        if (status === 400) {
          errors = JSON.parse(response['odata.error'].message.value);
          $scope.errors = errors;
        }
        return $rootScope.isloading = false;
      });
      return $scope.newtitle = "";
    };
    $scope.doDeleteCheckItems = function(todos) {
      var before_work, items;
      items = _.filter(todos, {
        'selected': true
      });
      before_work = function() {
        return $rootScope.isloading = true;
      };
      return repository.deleteTodos(items, before_work).then(function(response) {
        $rootScope.isloading = false;
        return initTodo();
      }, function(reason) {
        var status;
        status = reason.status;
        return $rootScope.isloading = false;
      });
    };
    return initTodo();
  }
];

},{"../common/filters":2}],8:[function(require,module,exports){
module.exports = [
  'api', '$q', function(api, $q) {
    var deferedTodos, doAddTodo, doDeleteTodo, doDeleteTodos, doInitTodos, doUpdateTodo, repository;
    deferedTodos = $q.defer();
    repository = {
      initTodos: function(before_works) {
        deferedTodos = $q.defer();
        before_works();
        doInitTodos();
        return deferedTodos.promise;
      },
      addTodo: function(data, before_works) {
        deferedTodos = $q.defer();
        before_works();
        doAddTodo(data);
        return deferedTodos.promise;
      },
      deleteTodo: function(data, before_works) {
        deferedTodos = $q.defer();
        before_works();
        doDeleteTodo(data);
        return deferedTodos.promise;
      },
      deleteTodos: function(datas, before_works) {
        deferedTodos = $q.defer();
        before_works();
        doDeleteTodos(datas);
        return deferedTodos.promise;
      },
      updateTodo: function(data, before_work) {
        deferedTodos = $q.defer();
        before_work();
        doUpdateTodo(data);
        return deferedTodos.promise;
      }
    };
    doInitTodos = function() {
      var onSuccess;
      onSuccess = function(data) {
        return deferedTodos.resolve(data.value);
      };
      return api.getTodos(onSuccess, (function() {}));
    };
    doAddTodo = function(data) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.addTodo(data, onSuccess, onError);
    };
    doDeleteTodo = function(data) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.deleteTodo(data, onSuccess, onError);
    };
    doDeleteTodos = function(datas) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.deleteTodos(datas, onSuccess, onError);
    };
    doUpdateTodo = function(data) {
      var onError, onSuccess;
      onSuccess = function(response) {
        return deferedTodos.resolve(response);
      };
      onError = function(response, status) {
        return deferedTodos.reject({
          response: response,
          status: status
        });
      };
      return api.updateTodo(data, onSuccess, onError);
    };
    return repository;
  }
];

},{}],9:[function(require,module,exports){
module.exports = [
  '$http', function($http) {
    var api;
    api = {
      getTodos: function(onSuccess, onError) {
        var url;
        url = '/odata/Todos';
        return $http.get(url).success(onSuccess).error(onError);
      },
      addTodo: function(data, onSuccess, onError) {
        var url;
        url = '/odata/Todos';
        return $http.post(url, data).success(onSuccess).error(onError);
      },
      deleteTodo: function(data, onSuccess, onError) {
        var url;
        url = '/odata/Todos';
        return $http["delete"](url).success(onSuccess).error(onError);
      },
      deleteTodos: function(datas, onSuccess, onError) {
        var keys, url;
        keys = _.map(datas, 'id');
        url = "/odata/Todos/Delete";
        return $http.post(url, {
          keys: keys
        }).success(onSuccess).error(onError);
      },
      updateTodo: function(data, onSuccess, onError) {
        var data_id, url;
        data_id = data.TodoId;
        url = "/odata/Todos(" + data_id + ")";
        return $http.put(url, data).success(onSuccess).error(onError);
      }
    };
    return api;
  }
];

},{}],10:[function(require,module,exports){

},{}]},{},[5])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkQ6XFxXb3Jrc1xcVGVzdFxcbXZjdG9kb1xcV2ViQXBwbGljYXRpb24zXFxub2RlX21vZHVsZXNcXGd1bHAtYnJvd3NlcmlmeVxcbm9kZV9tb2R1bGVzXFxicm93c2VyaWZ5XFxub2RlX21vZHVsZXNcXGJyb3dzZXItcGFja1xcX3ByZWx1ZGUuanMiLCJEOi9Xb3Jrcy9UZXN0L212Y3RvZG8vV2ViQXBwbGljYXRpb24zL2Fzc2V0cy9qcy9jb21tb24vZGlyZWN0aXZlcy5qcyIsIkQ6L1dvcmtzL1Rlc3QvbXZjdG9kby9XZWJBcHBsaWNhdGlvbjMvYXNzZXRzL2pzL2NvbW1vbi9maWx0ZXJzLmpzIiwiRDovV29ya3MvVGVzdC9tdmN0b2RvL1dlYkFwcGxpY2F0aW9uMy9hc3NldHMvanMvY29tbW9uL2luZGV4LmpzIiwiRDovV29ya3MvVGVzdC9tdmN0b2RvL1dlYkFwcGxpY2F0aW9uMy9hc3NldHMvanMvY29tbW9uL3V0aWxzLmpzIiwiRDovV29ya3MvVGVzdC9tdmN0b2RvL1dlYkFwcGxpY2F0aW9uMy9hc3NldHMvanMvZmFrZV9hYmYyNjkwNC5qcyIsIkQ6L1dvcmtzL1Rlc3QvbXZjdG9kby9XZWJBcHBsaWNhdGlvbjMvYXNzZXRzL2pzL3RvZG8vaW5kZXguanMiLCJEOi9Xb3Jrcy9UZXN0L212Y3RvZG8vV2ViQXBwbGljYXRpb24zL2Fzc2V0cy9qcy90b2RvL2xpc3QtY29udHJvbGxlci5qcyIsIkQ6L1dvcmtzL1Rlc3QvbXZjdG9kby9XZWJBcHBsaWNhdGlvbjMvYXNzZXRzL2pzL3RvZG8vbGlzdC1yZXBvc2l0b3J5LmpzIiwiRDovV29ya3MvVGVzdC9tdmN0b2RvL1dlYkFwcGxpY2F0aW9uMy9hc3NldHMvanMvdG9kby9saXN0LXNlcnZpY2UuanMiLCJEOi9Xb3Jrcy9UZXN0L212Y3RvZG8vV2ViQXBwbGljYXRpb24zL25vZGVfbW9kdWxlcy9ndWxwLWJyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbGliL19lbXB0eS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9DQTtBQUNBOztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbElBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKX12YXIgZj1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwoZi5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxmLGYuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwidmFyIHV0aWxzO1xuXG51dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcblxuZXhwb3J0cy5zZXJ2ZXJWYWxpZGF0ZWQgPSBmdW5jdGlvbigpIHtcbiAgcmV0dXJuIHtcbiAgICByZXN0cmljdDogJ0EnLFxuICAgIHJlcXVpcmU6ICc/bmdNb2RlbCcsXG4gICAgbGluazogZnVuY3Rpb24oc2NvcGUsIGVsZW1lbnQsIGF0dHJzLCBjdHJsKSB7XG4gICAgICBpZiAoZWxlbWVudC5wcm9wKCd0YWdOYW1lJykgPT09ICdGT1JNJykge1xuICAgICAgICByZXR1cm4gc2NvcGUuJHdhdGNoKFwiZXJyb3JzXCIsIGZ1bmN0aW9uKGVycm9ycykge1xuICAgICAgICAgIHJldHVybiB1dGlscy5wb3B1bGF0ZUZvcm1FcnJvcihlbGVtZW50LCBlcnJvcnMsIGVsZW1lbnQuYXR0cignbmFtZScpKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKGVsZW1lbnQucHJvcCgndGFnTmFtZScpID09PSAnRElWJykge1xuICAgICAgICByZXR1cm4gZWxlbWVudC5vbignY2xpY2sgZHJhZ2VudGVyJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgcmV0dXJuIHNjb3BlLiRhcHBseShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBjdHJsLiRzZXRWYWxpZGl0eSgnc2VydmVyJywgdHJ1ZSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHNjb3BlLiR3YXRjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICByZXR1cm4gY3RybC4kbW9kZWxWYWx1ZTtcbiAgICAgICAgfSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgY3RybC4kc2V0VmFsaWRpdHkoJ3NlcnZlcicsIHRydWUpO1xuICAgICAgICAgIGlmICghc2NvcGUuJCRwaGFzZSkge1xuICAgICAgICAgICAgcmV0dXJuIHNjb3BlLiRhcHBseSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9O1xufTtcblxuZXhwb3J0cy5lcnJvckZvciA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHJlcXVpcmU6ICdeZm9ybScsXG4gICAgdGVtcGxhdGU6ICd7e21lc3NhZ2V9fScsXG4gICAgc2NvcGU6IHt9LFxuICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtLCBhdHRycywgY3RybCkge1xuICAgICAgdmFyIGZpZWxkTmFtZSwgZm9ybU5hbWUsIHVwZGF0ZUVycm9yTWVzc2FnZTtcbiAgICAgIGZvcm1OYW1lID0gY3RybC4kbmFtZTtcbiAgICAgIGZpZWxkTmFtZSA9IGF0dHJzLmVycm9yRm9yO1xuICAgICAgZWxlbS5hZGRDbGFzcygnZXJyb3InKTtcbiAgICAgIHVwZGF0ZUVycm9yTWVzc2FnZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgZmllbGQ7XG4gICAgICAgIGZpZWxkID0gc2NvcGUuJHBhcmVudFtmb3JtTmFtZV1bZmllbGROYW1lXTtcbiAgICAgICAgaWYgKGZpZWxkICYmIGZpZWxkLiRpbnZhbGlkICYmIGZpZWxkLmVycm9yKSB7XG4gICAgICAgICAgZWxlbS5zaG93KCk7XG4gICAgICAgICAgcmV0dXJuIHNjb3BlLm1lc3NhZ2UgPSBmaWVsZC5lcnJvcjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBlbGVtLmhpZGUoKTtcbiAgICAgICAgICByZXR1cm4gc2NvcGUubWVzc2FnZSA9ICcnO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgcmV0dXJuIHNjb3BlLiR3YXRjaChcIiRwYXJlbnQuXCIgKyBmb3JtTmFtZSArIFwiLlwiICsgZmllbGROYW1lICsgXCIuJGVycm9yLnNlcnZlclwiLCB1cGRhdGVFcnJvck1lc3NhZ2UpO1xuICAgIH1cbiAgfTtcbn07XG5cbmV4cG9ydHMuY29uZmlybURpYWxvZyA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHByaW9yaXR5OiAtMSxcbiAgICByZXN0cmljdDogJ0EnLFxuICAgIGxpbms6IHtcbiAgICAgIHByZTogZnVuY3Rpb24oc2NvcGUsIGVsZW1lbnQsIGF0dHIpIHtcbiAgICAgICAgdmFyIG1zZztcbiAgICAgICAgbXNnID0gYXR0ci5jb25maXJtRGlhbG9nIHx8ICdBcmUgeW91IHN1cmU/JztcbiAgICAgICAgcmV0dXJuIGVsZW1lbnQuYmluZCgnY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgaWYgKCF3aW5kb3cuY29uZmlybShtc2cpKSB7XG4gICAgICAgICAgICBlLnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgcmV0dXJuIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfTtcbn07XG5cbmV4cG9ydHMuaGFzQ2hlY2tlZCA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4ge1xuICAgIHByaW9yaXR5OiAyLFxuICAgIHJlc3RyaWN0OiAnQScsXG4gICAgbGluazoge1xuICAgICAgcHJlOiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cikge1xuICAgICAgICByZXR1cm4gZWxlbWVudC5iaW5kKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICB2YXIgY291bnQsIGl0ZW1zO1xuICAgICAgICAgIGl0ZW1zID0gc2NvcGUuJGV2YWwoYXR0ci5oYXNDaGVja2VkKTtcbiAgICAgICAgICBjb3VudCA9IDA7XG4gICAgICAgICAgXy5lYWNoKGl0ZW1zLCBmdW5jdGlvbihpdGVtKSB7XG4gICAgICAgICAgICBpZiAoaXRlbVthdHRyLmhhc0NoZWNrZWRQcm9wXSkge1xuICAgICAgICAgICAgICByZXR1cm4gY291bnQgKz0gMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhjb3VudCk7XG4gICAgICAgICAgaWYgKGNvdW50ID09PSAwKSB7XG4gICAgICAgICAgICBhbGVydChhdHRyLmhhc0NoZWNrZWRQcm9tcHQpO1xuICAgICAgICAgICAgZS5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgIHJldHVybiBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH07XG59O1xuIiwiZXhwb3J0cy51dGNkYXRldGltZSA9IGZ1bmN0aW9uKGRhdGUpIHtcbiAgdmFyIG07XG4gIG0gPSBtb21lbnQudXRjKGRhdGUpO1xuICBpZiAobS5pc1ZhbGlkKCkpIHtcbiAgICByZXR1cm4gbS5mb3JtYXQoJ1lZWVktTU0tRERUSEg6bW06c3MnKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gZGF0ZTtcbiAgfVxufTtcbiIsInZhciBkaXJlY3RpdmVzO1xuXG5kaXJlY3RpdmVzID0gcmVxdWlyZSgnLi9kaXJlY3RpdmVzJyk7XG5cbmFuZ3VsYXIubW9kdWxlKCdjb21tb24nLCBbXSlcbiAgICAuZGlyZWN0aXZlKCdzZXJ2ZXJWYWxpZGF0ZWQnLCBkaXJlY3RpdmVzLnNlcnZlclZhbGlkYXRlZClcbiAgICAuZGlyZWN0aXZlKCdlcnJvckZvcicsIGRpcmVjdGl2ZXMuZXJyb3JGb3IpXG4gICAgLmRpcmVjdGl2ZSgnY29uZmlybURpYWxvZycsIGRpcmVjdGl2ZXMuY29uZmlybURpYWxvZylcbiAgICAuZGlyZWN0aXZlKCdoYXNDaGVja2VkJywgZGlyZWN0aXZlcy5oYXNDaGVja2VkKTtcbiIsIm1vZHVsZS5leHBvcnRzID0ge1xuICBwb3B1bGF0ZUZvcm1FcnJvcjogZnVuY3Rpb24oZm9ybUVsZW1lbnQsIGVycm9ycywgZm9ybU5hbWUsIGV4dGVybmFsRm9ybSkge1xuICAgIHZhciBmaWVsZEVycm9ycywgZmllbGROYW1lLCBmb3JtLCBpLCBpbmRleCwgaW52YWxpZEZvcm1FbGVtZW50cywgbGVuLCByZXNldEZvcm1Db250cm9sRXJyb3IsIHN1YmZvcm1FbGVtZW50LCBzdWJmb3JtRWxlbWVudHM7XG4gICAgaWYgKGZvcm1OYW1lID09IG51bGwpIHtcbiAgICAgIGZvcm1OYW1lID0gJ2Zvcm0nO1xuICAgIH1cbiAgICByZXNldEZvcm1Db250cm9sRXJyb3IgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBfLmZvck93bihmb3JtLCBmdW5jdGlvbihwcm9wKSB7XG4gICAgICAgIGlmIChwcm9wICYmIHByb3AuJG5hbWUpIHtcbiAgICAgICAgICBpZiAoKCFfLmhhcyhlcnJvcnMsIHByb3AuJG5hbWUpKSAmJiBfLmhhcyhmb3JtW3Byb3AuJG5hbWVdLCAnZXJyb3InKSkge1xuICAgICAgICAgICAgZGVsZXRlIGZvcm1bcHJvcC4kbmFtZV0uZXJyb3I7XG4gICAgICAgICAgICByZXR1cm4gZm9ybVtwcm9wLiRuYW1lXS4kc2V0VmFsaWRpdHkoJ3NlcnZlcicsIHRydWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfTtcbiAgICBleHRlcm5hbEZvcm0gPSBleHRlcm5hbEZvcm0gfHwgZm9ybUVsZW1lbnQ7XG4gICAgZm9ybSA9IGZvcm1FbGVtZW50LnNjb3BlKClbZm9ybU5hbWVdO1xuICAgIHJlc2V0Rm9ybUNvbnRyb2xFcnJvcigpO1xuICAgIGZvciAoZmllbGROYW1lIGluIGVycm9ycykge1xuICAgICAgZmllbGRFcnJvcnMgPSBlcnJvcnNbZmllbGROYW1lXTtcbiAgICAgIGlmIChmaWVsZEVycm9ycy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICBpZiAoYW5ndWxhci5pc09iamVjdChmaWVsZEVycm9yc1swXSkpIHtcbiAgICAgICAgc3ViZm9ybUVsZW1lbnRzID0gYW5ndWxhci5lbGVtZW50KFwiW25nLWZvcm09J1wiICsgZmllbGROYW1lICsgXCInXVwiLCBmb3JtRWxlbWVudCk7XG4gICAgICAgIGZvciAoaW5kZXggPSBpID0gMCwgbGVuID0gc3ViZm9ybUVsZW1lbnRzLmxlbmd0aDsgaSA8IGxlbjsgaW5kZXggPSArK2kpIHtcbiAgICAgICAgICBzdWJmb3JtRWxlbWVudCA9IHN1YmZvcm1FbGVtZW50c1tpbmRleF07XG4gICAgICAgICAgdGhpcy5wb3B1bGF0ZUZvcm1FcnJvcihhbmd1bGFyLmVsZW1lbnQoc3ViZm9ybUVsZW1lbnQpLCBmaWVsZEVycm9yc1tpbmRleF0sIGZpZWxkTmFtZSwgZXh0ZXJuYWxGb3JtKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCFmb3JtW2ZpZWxkTmFtZV0pIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBmb3JtW2ZpZWxkTmFtZV0uJHNldFZhbGlkaXR5KCdzZXJ2ZXInLCBmYWxzZSk7XG4gICAgICAgIGZvcm1bZmllbGROYW1lXS5lcnJvciA9IGZpZWxkRXJyb3JzWzBdO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAoZXh0ZXJuYWxGb3JtICE9PSBmb3JtRWxlbWVudCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBpbnZhbGlkRm9ybUVsZW1lbnRzID0gJChcIjpub3QoZm9ybSxuZ0Zvcm0sW25nLWZvcm1dKS5uZy1pbnZhbGlkXCIsIGZvcm1FbGVtZW50KTtcbiAgICBpZiAoaW52YWxpZEZvcm1FbGVtZW50cy5sZW5ndGggPiAwKSB7XG4gICAgICByZXR1cm4gaW52YWxpZEZvcm1FbGVtZW50c1swXS5mb2N1cygpO1xuICAgIH1cbiAgfVxufTtcbiIsInJlcXVpcmUoXCIuL3RvZG9cIik7XG4iLCJyZXF1aXJlKCcuLi9jb21tb24nKTtcblxuYW5ndWxhci5tb2R1bGUoXCJ0b2RvXCIsIFsnY29tbW9uJ10pXG4gICAgLnNlcnZpY2UoJ2FwaScsIHJlcXVpcmUoJy4vbGlzdC1zZXJ2aWNlJykpXG4gICAgLmZhY3RvcnkoJ3JlcG9zaXRvcnknLCByZXF1aXJlKCcuL2xpc3QtcmVwb3NpdG9yeScpKVxuICAgIC5jb250cm9sbGVyKFwiTGlzdENvbnRyb2xsZXJcIiwgcmVxdWlyZShcIi4vbGlzdC1jb250cm9sbGVyXCIpKTtcbiIsInZhciBfLCBmaWx0ZXJzO1xuXG5fID0gcmVxdWlyZSgnbG9kYXNoJyk7XG5cbmZpbHRlcnMgPSByZXF1aXJlKCcuLi9jb21tb24vZmlsdGVycycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFtcbiAgJyRyb290U2NvcGUnLCAnJHNjb3BlJywgJyRxJywgJ3JlcG9zaXRvcnknLCBmdW5jdGlvbigkcm9vdFNjb3BlLCAkc2NvcGUsICRxLCByZXBvc2l0b3J5KSB7XG4gICAgdmFyIGluaXRUb2RvLCBsYXN0SWQsIG1ha2VUb2RvO1xuICAgIGxhc3RJZCA9IDA7XG4gICAgJHNjb3BlLnRvZG9zID0gW107XG4gICAgJHNjb3BlLmVycm9ycyA9IHt9O1xuICAgIG1ha2VUb2RvID0gZnVuY3Rpb24oaWQsIHRpdGxlLCBjcmVhdGVkLCB1cGRhdGVkLCBzZWxlY3RlZCkge1xuICAgICAgdmFyIGl0ZW07XG4gICAgICBpZiAoc2VsZWN0ZWQgPT09IHZvaWQgMCkge1xuICAgICAgICBzZWxlY3RlZCA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgaXRlbSA9IHtcbiAgICAgICAgaWQ6IGlkLFxuICAgICAgICB0aXRsZTogdGl0bGUsXG4gICAgICAgIGNyZWF0ZWQ6IGNyZWF0ZWQsXG4gICAgICAgIHVwZGF0ZWQ6IHVwZGF0ZWQsXG4gICAgICAgIGlzZWRpdDogZmFsc2UsXG4gICAgICAgIHNlbGVjdGVkOiBzZWxlY3RlZFxuICAgICAgfTtcbiAgICAgIGxhc3RJZCA9IGlkICsgMTtcbiAgICAgIHJldHVybiBpdGVtO1xuICAgIH07XG4gICAgaW5pdFRvZG8gPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiByZXBvc2l0b3J5LmluaXRUb2RvcyhmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuICRyb290U2NvcGUuaXNsb2FkaW5nID0gdHJ1ZTtcbiAgICAgIH0pLnRoZW4oZnVuY3Rpb24odG9kb3MpIHtcbiAgICAgICAgJHNjb3BlLnRvZG9zID0gW107XG4gICAgICAgIF8uZWFjaCh0b2RvcywgZnVuY3Rpb24odG9kbykge1xuICAgICAgICAgIHJldHVybiAkc2NvcGUudG9kb3MucHVzaChtYWtlVG9kbyh0b2RvLlRvZG9JZCwgdG9kby5UaXRsZSwgdG9kby5DcmVhdGVkLCB0b2RvLlVwZGF0ZWQsIHRvZG8uQ2hlY2tlZCkpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuICRyb290U2NvcGUuaXNsb2FkaW5nID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9O1xuICAgICRzY29wZS5kb0VkaXRNb2RlID0gZnVuY3Rpb24oaXRlbSkge1xuICAgICAgaXRlbS5pc2VkaXQgPSAhaXRlbS5pc2VkaXQ7XG4gICAgICByZXR1cm4gaXRlbS5jb3B5X3RpdGxlID0gYW5ndWxhci5jb3B5KGl0ZW0udGl0bGUpO1xuICAgIH07XG4gICAgJHNjb3BlLmRvVXBkYXRlRWRpdCA9IGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgIHZhciBiZWZvcmVfd29yaywgZGF0YTtcbiAgICAgIGlmIChpdGVtLnRpdGxlID09PSBpdGVtLmNvcHlfdGl0bGUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgZGF0YSA9IHtcbiAgICAgICAgVG9kb0lkOiBpdGVtLmlkLFxuICAgICAgICBUaXRsZTogaXRlbS5jb3B5X3RpdGxlXG4gICAgICB9O1xuICAgICAgYmVmb3JlX3dvcmsgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuICRyb290U2NvcGUuaXNsb2FkaW5nID0gdHJ1ZTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gcmVwb3NpdG9yeS51cGRhdGVUb2RvKGRhdGEsIGJlZm9yZV93b3JrKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgIGl0ZW0uaXNlZGl0ID0gZmFsc2U7XG4gICAgICAgIGl0ZW0udGl0bGUgPSByZXNwb25zZS5UaXRsZTtcbiAgICAgICAgaXRlbS51cGRhdGVkID0gcmVzcG9uc2UuVXBkYXRlZDtcbiAgICAgICAgcmV0dXJuICRyb290U2NvcGUuaXNsb2FkaW5nID0gZmFsc2U7XG4gICAgICB9LCBmdW5jdGlvbihyZWFzb24pIHtcbiAgICAgICAgdmFyIHN0YXR1cztcbiAgICAgICAgc3RhdHVzID0gcmVhc29uLnN0YXR1cztcbiAgICAgICAgaWYgKHN0YXR1cyA9PT0gNDA0KSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ1Jlc291cmNlIG5vdCBmb3VuZC4nKTtcbiAgICAgICAgfVxuICAgICAgICBpdGVtLmlzZWRpdCA9IGZhbHNlO1xuICAgICAgICByZXR1cm4gJHJvb3RTY29wZS5pc2xvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH07XG4gICAgJHNjb3BlLmRvRGVsZXRlID0gZnVuY3Rpb24oaXRlbSkge1xuICAgICAgdmFyIGJlZm9yZV93b3JrLCBpbmRleDtcbiAgICAgIGluZGV4ID0gJHNjb3BlLnRvZG9zLmluZGV4T2YoaXRlbSk7XG4gICAgICBiZWZvcmVfd29yayA9IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gJHJvb3RTY29wZS5pc2xvYWRpbmcgPSB0cnVlO1xuICAgICAgfTtcbiAgICAgIHJldHVybiByZXBvc2l0b3J5LmRlbGV0ZVRvZG8oaXRlbSwgYmVmb3JlX3dvcmspLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgJHNjb3BlLnRvZG9zLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgIHJldHVybiAkcm9vdFNjb3BlLmlzbG9hZGluZyA9IGZhbHNlO1xuICAgICAgfSwgZnVuY3Rpb24ocmVhc29uKSB7XG4gICAgICAgIHZhciBzdGF0dXM7XG4gICAgICAgIHN0YXR1cyA9IHJlYXNvbi5zdGF0dXM7XG4gICAgICAgIGlmIChzdGF0dXMgPT09IDQwNCkge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdSZXNvdXJjZSBub3QgZm91bmQnKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gJHJvb3RTY29wZS5pc2xvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH07XG4gICAgJHNjb3BlLmRvQWRkID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgYmVmb3JlX3dvcmssIGRhdGE7XG4gICAgICAkcm9vdFNjb3BlLmlzbG9hZGluZyA9IHRydWU7XG4gICAgICBkYXRhID0ge1xuICAgICAgICBUaXRsZTogJHNjb3BlLm5ld3RpdGxlXG4gICAgICB9O1xuICAgICAgYmVmb3JlX3dvcmsgPSAoZnVuY3Rpb24oKSB7fSk7XG4gICAgICByZXBvc2l0b3J5LmFkZFRvZG8oZGF0YSwgYmVmb3JlX3dvcmspLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgJHNjb3BlLnRvZG9zLnVuc2hpZnQobWFrZVRvZG8ocmVzcG9uc2UuVG9kb0lkLCByZXNwb25zZS5UaXRsZSwgcmVzcG9uc2UuQ3JlYXRlZCwgcmVzcG9uc2UuVXBkYXRlZCwgcmVzcG9uc2UuQ2hlY2tlZCkpO1xuICAgICAgICByZXR1cm4gJHJvb3RTY29wZS5pc2xvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0sIGZ1bmN0aW9uKHJlYXNvbikge1xuICAgICAgICB2YXIgZXJyb3JzLCByZXNwb25zZSwgc3RhdHVzO1xuICAgICAgICByZXNwb25zZSA9IHJlYXNvbi5yZXNwb25zZTtcbiAgICAgICAgc3RhdHVzID0gcmVhc29uLnN0YXR1cztcbiAgICAgICAgaWYgKHN0YXR1cyA9PT0gNDAwKSB7XG4gICAgICAgICAgZXJyb3JzID0gSlNPTi5wYXJzZShyZXNwb25zZVsnb2RhdGEuZXJyb3InXS5tZXNzYWdlLnZhbHVlKTtcbiAgICAgICAgICAkc2NvcGUuZXJyb3JzID0gZXJyb3JzO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAkcm9vdFNjb3BlLmlzbG9hZGluZyA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gJHNjb3BlLm5ld3RpdGxlID0gXCJcIjtcbiAgICB9O1xuICAgICRzY29wZS5kb0RlbGV0ZUNoZWNrSXRlbXMgPSBmdW5jdGlvbih0b2Rvcykge1xuICAgICAgdmFyIGJlZm9yZV93b3JrLCBpdGVtcztcbiAgICAgIGl0ZW1zID0gXy5maWx0ZXIodG9kb3MsIHtcbiAgICAgICAgJ3NlbGVjdGVkJzogdHJ1ZVxuICAgICAgfSk7XG4gICAgICBiZWZvcmVfd29yayA9IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gJHJvb3RTY29wZS5pc2xvYWRpbmcgPSB0cnVlO1xuICAgICAgfTtcbiAgICAgIHJldHVybiByZXBvc2l0b3J5LmRlbGV0ZVRvZG9zKGl0ZW1zLCBiZWZvcmVfd29yaykudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAkcm9vdFNjb3BlLmlzbG9hZGluZyA9IGZhbHNlO1xuICAgICAgICByZXR1cm4gaW5pdFRvZG8oKTtcbiAgICAgIH0sIGZ1bmN0aW9uKHJlYXNvbikge1xuICAgICAgICB2YXIgc3RhdHVzO1xuICAgICAgICBzdGF0dXMgPSByZWFzb24uc3RhdHVzO1xuICAgICAgICByZXR1cm4gJHJvb3RTY29wZS5pc2xvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH07XG4gICAgcmV0dXJuIGluaXRUb2RvKCk7XG4gIH1cbl07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IFtcbiAgJ2FwaScsICckcScsIGZ1bmN0aW9uKGFwaSwgJHEpIHtcbiAgICB2YXIgZGVmZXJlZFRvZG9zLCBkb0FkZFRvZG8sIGRvRGVsZXRlVG9kbywgZG9EZWxldGVUb2RvcywgZG9Jbml0VG9kb3MsIGRvVXBkYXRlVG9kbywgcmVwb3NpdG9yeTtcbiAgICBkZWZlcmVkVG9kb3MgPSAkcS5kZWZlcigpO1xuICAgIHJlcG9zaXRvcnkgPSB7XG4gICAgICBpbml0VG9kb3M6IGZ1bmN0aW9uKGJlZm9yZV93b3Jrcykge1xuICAgICAgICBkZWZlcmVkVG9kb3MgPSAkcS5kZWZlcigpO1xuICAgICAgICBiZWZvcmVfd29ya3MoKTtcbiAgICAgICAgZG9Jbml0VG9kb3MoKTtcbiAgICAgICAgcmV0dXJuIGRlZmVyZWRUb2Rvcy5wcm9taXNlO1xuICAgICAgfSxcbiAgICAgIGFkZFRvZG86IGZ1bmN0aW9uKGRhdGEsIGJlZm9yZV93b3Jrcykge1xuICAgICAgICBkZWZlcmVkVG9kb3MgPSAkcS5kZWZlcigpO1xuICAgICAgICBiZWZvcmVfd29ya3MoKTtcbiAgICAgICAgZG9BZGRUb2RvKGRhdGEpO1xuICAgICAgICByZXR1cm4gZGVmZXJlZFRvZG9zLnByb21pc2U7XG4gICAgICB9LFxuICAgICAgZGVsZXRlVG9kbzogZnVuY3Rpb24oZGF0YSwgYmVmb3JlX3dvcmtzKSB7XG4gICAgICAgIGRlZmVyZWRUb2RvcyA9ICRxLmRlZmVyKCk7XG4gICAgICAgIGJlZm9yZV93b3JrcygpO1xuICAgICAgICBkb0RlbGV0ZVRvZG8oZGF0YSk7XG4gICAgICAgIHJldHVybiBkZWZlcmVkVG9kb3MucHJvbWlzZTtcbiAgICAgIH0sXG4gICAgICBkZWxldGVUb2RvczogZnVuY3Rpb24oZGF0YXMsIGJlZm9yZV93b3Jrcykge1xuICAgICAgICBkZWZlcmVkVG9kb3MgPSAkcS5kZWZlcigpO1xuICAgICAgICBiZWZvcmVfd29ya3MoKTtcbiAgICAgICAgZG9EZWxldGVUb2RvcyhkYXRhcyk7XG4gICAgICAgIHJldHVybiBkZWZlcmVkVG9kb3MucHJvbWlzZTtcbiAgICAgIH0sXG4gICAgICB1cGRhdGVUb2RvOiBmdW5jdGlvbihkYXRhLCBiZWZvcmVfd29yaykge1xuICAgICAgICBkZWZlcmVkVG9kb3MgPSAkcS5kZWZlcigpO1xuICAgICAgICBiZWZvcmVfd29yaygpO1xuICAgICAgICBkb1VwZGF0ZVRvZG8oZGF0YSk7XG4gICAgICAgIHJldHVybiBkZWZlcmVkVG9kb3MucHJvbWlzZTtcbiAgICAgIH1cbiAgICB9O1xuICAgIGRvSW5pdFRvZG9zID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgb25TdWNjZXNzO1xuICAgICAgb25TdWNjZXNzID0gZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICByZXR1cm4gZGVmZXJlZFRvZG9zLnJlc29sdmUoZGF0YS52YWx1ZSk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIGFwaS5nZXRUb2RvcyhvblN1Y2Nlc3MsIChmdW5jdGlvbigpIHt9KSk7XG4gICAgfTtcbiAgICBkb0FkZFRvZG8gPSBmdW5jdGlvbihkYXRhKSB7XG4gICAgICB2YXIgb25FcnJvciwgb25TdWNjZXNzO1xuICAgICAgb25TdWNjZXNzID0gZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgcmV0dXJuIGRlZmVyZWRUb2Rvcy5yZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgIH07XG4gICAgICBvbkVycm9yID0gZnVuY3Rpb24ocmVzcG9uc2UsIHN0YXR1cykge1xuICAgICAgICByZXR1cm4gZGVmZXJlZFRvZG9zLnJlamVjdCh7XG4gICAgICAgICAgcmVzcG9uc2U6IHJlc3BvbnNlLFxuICAgICAgICAgIHN0YXR1czogc3RhdHVzXG4gICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBhcGkuYWRkVG9kbyhkYXRhLCBvblN1Y2Nlc3MsIG9uRXJyb3IpO1xuICAgIH07XG4gICAgZG9EZWxldGVUb2RvID0gZnVuY3Rpb24oZGF0YSkge1xuICAgICAgdmFyIG9uRXJyb3IsIG9uU3VjY2VzcztcbiAgICAgIG9uU3VjY2VzcyA9IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgIHJldHVybiBkZWZlcmVkVG9kb3MucmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICB9O1xuICAgICAgb25FcnJvciA9IGZ1bmN0aW9uKHJlc3BvbnNlLCBzdGF0dXMpIHtcbiAgICAgICAgcmV0dXJuIGRlZmVyZWRUb2Rvcy5yZWplY3Qoe1xuICAgICAgICAgIHJlc3BvbnNlOiByZXNwb25zZSxcbiAgICAgICAgICBzdGF0dXM6IHN0YXR1c1xuICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gYXBpLmRlbGV0ZVRvZG8oZGF0YSwgb25TdWNjZXNzLCBvbkVycm9yKTtcbiAgICB9O1xuICAgIGRvRGVsZXRlVG9kb3MgPSBmdW5jdGlvbihkYXRhcykge1xuICAgICAgdmFyIG9uRXJyb3IsIG9uU3VjY2VzcztcbiAgICAgIG9uU3VjY2VzcyA9IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgIHJldHVybiBkZWZlcmVkVG9kb3MucmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICB9O1xuICAgICAgb25FcnJvciA9IGZ1bmN0aW9uKHJlc3BvbnNlLCBzdGF0dXMpIHtcbiAgICAgICAgcmV0dXJuIGRlZmVyZWRUb2Rvcy5yZWplY3Qoe1xuICAgICAgICAgIHJlc3BvbnNlOiByZXNwb25zZSxcbiAgICAgICAgICBzdGF0dXM6IHN0YXR1c1xuICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gYXBpLmRlbGV0ZVRvZG9zKGRhdGFzLCBvblN1Y2Nlc3MsIG9uRXJyb3IpO1xuICAgIH07XG4gICAgZG9VcGRhdGVUb2RvID0gZnVuY3Rpb24oZGF0YSkge1xuICAgICAgdmFyIG9uRXJyb3IsIG9uU3VjY2VzcztcbiAgICAgIG9uU3VjY2VzcyA9IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgIHJldHVybiBkZWZlcmVkVG9kb3MucmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICB9O1xuICAgICAgb25FcnJvciA9IGZ1bmN0aW9uKHJlc3BvbnNlLCBzdGF0dXMpIHtcbiAgICAgICAgcmV0dXJuIGRlZmVyZWRUb2Rvcy5yZWplY3Qoe1xuICAgICAgICAgIHJlc3BvbnNlOiByZXNwb25zZSxcbiAgICAgICAgICBzdGF0dXM6IHN0YXR1c1xuICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gYXBpLnVwZGF0ZVRvZG8oZGF0YSwgb25TdWNjZXNzLCBvbkVycm9yKTtcbiAgICB9O1xuICAgIHJldHVybiByZXBvc2l0b3J5O1xuICB9XG5dO1xuIiwibW9kdWxlLmV4cG9ydHMgPSBbXG4gICckaHR0cCcsIGZ1bmN0aW9uKCRodHRwKSB7XG4gICAgdmFyIGFwaTtcbiAgICBhcGkgPSB7XG4gICAgICBnZXRUb2RvczogZnVuY3Rpb24ob25TdWNjZXNzLCBvbkVycm9yKSB7XG4gICAgICAgIHZhciB1cmw7XG4gICAgICAgIHVybCA9ICcvb2RhdGEvVG9kb3MnO1xuICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KHVybCkuc3VjY2VzcyhvblN1Y2Nlc3MpLmVycm9yKG9uRXJyb3IpO1xuICAgICAgfSxcbiAgICAgIGFkZFRvZG86IGZ1bmN0aW9uKGRhdGEsIG9uU3VjY2Vzcywgb25FcnJvcikge1xuICAgICAgICB2YXIgdXJsO1xuICAgICAgICB1cmwgPSAnL29kYXRhL1RvZG9zJztcbiAgICAgICAgcmV0dXJuICRodHRwLnBvc3QodXJsLCBkYXRhKS5zdWNjZXNzKG9uU3VjY2VzcykuZXJyb3Iob25FcnJvcik7XG4gICAgICB9LFxuICAgICAgZGVsZXRlVG9kbzogZnVuY3Rpb24oZGF0YSwgb25TdWNjZXNzLCBvbkVycm9yKSB7XG4gICAgICAgIHZhciB1cmw7XG4gICAgICAgIHVybCA9ICcvb2RhdGEvVG9kb3MnO1xuICAgICAgICByZXR1cm4gJGh0dHBbXCJkZWxldGVcIl0odXJsKS5zdWNjZXNzKG9uU3VjY2VzcykuZXJyb3Iob25FcnJvcik7XG4gICAgICB9LFxuICAgICAgZGVsZXRlVG9kb3M6IGZ1bmN0aW9uKGRhdGFzLCBvblN1Y2Nlc3MsIG9uRXJyb3IpIHtcbiAgICAgICAgdmFyIGtleXMsIHVybDtcbiAgICAgICAga2V5cyA9IF8ubWFwKGRhdGFzLCAnaWQnKTtcbiAgICAgICAgdXJsID0gXCIvb2RhdGEvVG9kb3MvRGVsZXRlXCI7XG4gICAgICAgIHJldHVybiAkaHR0cC5wb3N0KHVybCwge1xuICAgICAgICAgIGtleXM6IGtleXNcbiAgICAgICAgfSkuc3VjY2VzcyhvblN1Y2Nlc3MpLmVycm9yKG9uRXJyb3IpO1xuICAgICAgfSxcbiAgICAgIHVwZGF0ZVRvZG86IGZ1bmN0aW9uKGRhdGEsIG9uU3VjY2Vzcywgb25FcnJvcikge1xuICAgICAgICB2YXIgZGF0YV9pZCwgdXJsO1xuICAgICAgICBkYXRhX2lkID0gZGF0YS5Ub2RvSWQ7XG4gICAgICAgIHVybCA9IFwiL29kYXRhL1RvZG9zKFwiICsgZGF0YV9pZCArIFwiKVwiO1xuICAgICAgICByZXR1cm4gJGh0dHAucHV0KHVybCwgZGF0YSkuc3VjY2VzcyhvblN1Y2Nlc3MpLmVycm9yKG9uRXJyb3IpO1xuICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuIGFwaTtcbiAgfVxuXTtcbiIsbnVsbF19
