﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.DynamicData;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using System.Web.UI.WebControls;
using Microsoft.Data.OData;
using Newtonsoft.Json;
using WebApplication3.Models;
using WebApplication3Lib.Extensions;
using WebApplication3Lib.Models;

namespace WebApplication3.Controllers
{
    public class TodosController : ODataController
    {
        private TodoContext db = new TodoContext();

        [HttpGet]
        public IQueryable<Models.Todo> Get()
        {
            //var db = new Models.TodoContext();
            var todos = db.Todos.Where(t => t.Mark == TodoMarkEnums.Normal).OrderByDescending(t => t.TodoId);
            return todos;
        }

        //[HttpGet]
        //public HttpResponseMessage Get([FromODataUri] int key)
        //{
        //    var todo = db.Todos.SingleOrDefault(t => t.TodoId == key);
        //    if (todo == null)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.NotFound);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, todo);
        //}

        [HttpPost]
        public HttpResponseMessage Post([FromBody] Todo todo)
        {
            HttpResponseMessage response;

            if (todo == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (ModelState.IsValid == false)
            {
                var errorList = ModelState.ToDictionary(
                    kvp => (kvp.Key.GetFieldName()),
                    kvp => kvp.Value.Errors.Select(e => e.ErrorMessage)
                );
                var s = JsonConvert.SerializeObject(errorList);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, s);
            }

            try
            {
                todo.Created = DateTime.UtcNow;
                todo.Mark = TodoMarkEnums.Normal;

                db.Todos.Add(todo);
                db.SaveChanges();

                response = Request.CreateResponse(HttpStatusCode.Created, todo);
                response.Headers.Add("Location", Url.CreateODataLink(new EntitySetPathSegment("Todos")));
                return response;
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                return response;
            }
        }

        public HttpResponseMessage Put([FromODataUri]int key, [FromBody] Todo todo)
        {
            var todoToUpdate = db.Todos.SingleOrDefault(t => t.TodoId == key);

            if (todoToUpdate == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                todoToUpdate.Title = todo.Title;
                todoToUpdate.Checked = todo.Checked;
                todoToUpdate.Updated = DateTime.UtcNow;

                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, todoToUpdate);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpDelete]
        public HttpResponseMessage Delete([FromODataUri]int key)
        {
            var todo = db.Todos.FirstOrDefault(t => t.TodoId == key);
            if (todo == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                db.Todos.Remove(todo);
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage Delete(ODataActionParameters parameters)
        {
            var keyeEumers = parameters["keys"] as IEnumerable<int>;
            var keys = keyeEumers.ToList();

            var todos = db.Todos.Where(t => keys.Contains(t.TodoId)).OrderByDescending(t => t.Created)
                .ToList();

            try
            {
                foreach (var todo in todos)
                {
                    todo.Mark = TodoMarkEnums.Deleted;
                }

                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
