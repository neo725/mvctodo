﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Spatial;
using System.Web.Http.ModelBinding;
using WebApplication3Lib.Models;
using WebApplication3.App_LocalResources;

namespace WebApplication3.Models
{
    public class TodoContext : DbContext
    {
        public DbSet<Todo> Todos { get; set; }
    }

    [Table("Todo")]
    public class Todo
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public Todo()
        //{
            
        //}
        [Key]
        public int TodoId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ModelResource),
        //    ErrorMessageResourceName = "TodoTitleMustBeRequired")]
        [Required(ErrorMessage = "[[[標題為必填欄位]]]")]
        [StringLength(100)]
        public string Title { get; set; }
        
        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public bool Checked { get; set; }

        [DefaultValue(TodoMarkEnums.Normal)]
        public TodoMarkEnums Mark { get; set; }
    }
}