msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2016-01-27 19:31+08:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: i18n.POTGenerator\n"

#: Views\Lang\Index.cshtml:24
msgid "Auto"
msgstr ""

#: Views\Lang\Index.cshtml:23
msgid "Browser default language setting"
msgstr ""

#: Controllers\IndexController.cs:14
msgid "Todo"
msgstr ""

#: Views\Index\Index.cshtml:31
msgid "必須選取至少一個項目"
msgstr ""

#: Views\Index\Index.cshtml:32
#: Views\Index\Index.cshtml:52
msgid "刪除"
msgstr ""

#: Views\Index\Index.cshtml:53
msgid "更新"
msgstr ""

#: Views\Index\Index.cshtml:54
msgid "取消"
msgstr ""

#: Views\Index\Index.cshtml:51
msgid "修改"
msgstr ""

#: Views\Index\Index.cshtml:34
msgid "項目"
msgstr ""

#: Models\Todo.cs:34
msgid "標題為必填欄位"
msgstr ""

#: Views\Index\Index.cshtml:32
msgid "確定要刪除這些選取的項目?"
msgstr ""

#: Views\Index\Index.cshtml:52
msgid "確定要刪除這個項目?"
msgstr ""

