﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using WebApplication3.Models;

namespace WebApplication3
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 設定和服務

            //// Web API 路由
            //config.MapHttpAttributeRoutes();

            // 1. 啟用OData篩選器
            // https://msdn.microsoft.com/zh-tw/library/gg309461%28v=crm.7%29.aspx
            config.AddODataQueryFilter();

            //// 2. 移除預設的xml輸出格式
            //var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            //config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            //// 3. 將api輸出的資料以camelCase更名
            //var jsonFormatter = config.Formatters.OfType<System.Net.Http.Formatting.JsonMediaTypeFormatter>().First();
            //jsonFormatter.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();

            //// 4. 將所有遞迴參考轉換成json關聯($ref => $id)
            //config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize;
            //config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;

            // OData for Employee test
            var builder = new ODataConventionModelBuilder();
            //modelBuilder.EntitySet<Employee>("Employees");
            //builder.Namespace = "NS";
            builder.EntitySet<Todo>("Todos");
            builder.Entity<Todo>().Collection
                .Action("Delete")
                .CollectionParameter<int>("keys");
            
            Microsoft.Data.Edm.IEdmModel model = builder.GetEdmModel();

            config.Routes.MapODataServiceRoute("ODataRoute", "odata", model);

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
