namespace WebApplication3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUpdatedfield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Todo", "Updated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Todo", "Updated");
        }
    }
}
