using WebApplication3Lib.Models;

namespace WebApplication3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTodoMarkfieldandEnum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Todo", "Mark", c => c.Int(nullable: false, defaultValueSql: Convert.ToString((int)TodoMarkEnums.Normal)));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Todo", "Mark");
        }
    }
}
