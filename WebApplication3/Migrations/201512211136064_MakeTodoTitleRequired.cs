namespace WebApplication3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeTodoTitleRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Todo", "Title", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Todo", "Title", c => c.String(maxLength: 100));
        }
    }
}
