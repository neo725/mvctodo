using WebApplication3.Models;
using WebApplication3Lib.Models;

namespace WebApplication3.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApplication3.Models.TodoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebApplication3.Models.TodoContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Todos.AddOrUpdate(
                t => t.Title,
                new Todo {Title = "Test 1", Created = DateTime.UtcNow.AddHours(-1), Checked = false, Mark = TodoMarkEnums.Normal},
                new Todo {Title = "Test 2", Created = DateTime.UtcNow, Checked = false, Mark = TodoMarkEnums.Normal});
        }
    }
}
