var gulp = require("gulp"),
    sass = require("gulp-sass"),
    minifycss = require('gulp-minify-css'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    gif = require("gulp-if"),
    clean = require("gulp-clean"),
    sequence = require('run-sequence'),
    browserify = require('gulp-browserify'),
    coffee = require('gulp-coffee'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    gdebug = require('gulp-debug');

var isMinifyCss = true,
    isUglifyJs = false,
    debug = true;

gulp.task("clean", function () {
    return gulp.src(["assets/static", "assets/build"], {read: false})
        .pipe(clean({force: true}));
});

gulp.task("css", function(){
    sequence("css:tocss", "css:minify");
});

gulp.task("css:tocss", function(){
    return gulp.src('assets/scss/module/*.scss')
        .pipe(sass())
        .pipe(rename(function(path){
            path.extname = ".css";
        }))
        .pipe(gulp.dest('assets/build/css'));
});

gulp.task("css:minify", function(){
    return gulp.src('assets/build/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(gif(isMinifyCss, minifycss()))
        .pipe(rename(function(path) {
            if (isMinifyCss)
                path.basename += ".min";
            path.extname = ".css";
        }))
        .pipe(concat('site.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/static/css'));
});

gulp.task('vendorjs', function () {
    return gulp.src(['assets/js/vendor.js'])
        .pipe(browserify({
            insertGlobals: false,
            debug: debug,
            shim: {
                jquery: {
                    path: 'bower_components/jquery/dist/jquery.js',
                    exports: '$'
                },
                angular: {
                    path: 'bower_components/angular/angular.js',
                    exports: 'angular',
                    depends: {
                        jquery: '$'
                    }
                },
                lodash: {
                    path: 'bower_components/lodash/lodash.js',
                    exports: '_'
                },
                moment: {
                    path: 'bower_components/moment/min/moment.min.js',
                    exports: 'moment'
                }
            }
        }))
        .pipe(gif(isUglifyJs, uglify()))
        .pipe(gulp.dest('assets/static/js'));
});

//gulp.task('coffeeifyjs', function () {
//    return gulp.src('assets/coffee/**/*.coffee')
//        .pipe(coffee({bare: true}).on('error', gutil.log))
//        .pipe(gulp.dest('assets/build/js'))
//});

gulp.task('appjs', function () {
    return gulp.src('assets/js/**/main-*.js')
        .pipe(browserify({
            insertGlobals: false,
            debug: debug,
            external: ['jquery', 'angular', 'lodash']
        }))
        .pipe(gif(isUglifyJs, uglify({mangle: false})))
        .pipe(gulp.dest('assets/static/js'));
});

gulp.task("watch", function() {
    gulp.watch(['assets/js/**/*.js'], function(){
        sequence("appjs");
    });
    gulp.watch(['assets/scss/**/*.scss'], ['css']);
});

gulp.task("js", function() {
    sequence("vendorjs", "appjs");
});

gulp.task("assets", ["js", "css"]);

gulp.task("default", function() {
	sequence("clean", "assets", "watch");
});