﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebApplication3Lib.Extensions
{
    public static class StringRegexExtensions
    {
        static readonly Regex regexODataFieldName = new Regex(@"(\w+)\.(\w+)");

        public static string GetFieldName(this string fieldName)
        {
            Match match = regexODataFieldName.Match(fieldName);

            if (match.Success == false)
            {
                return fieldName;
            }
            if (match.Groups.Count < 3)
            {
                return fieldName;
            }

            var group = String.Empty;
            for (var i = 2; i < match.Groups.Count; i++)
            {
                group += match.Groups[i];
                if (i < match.Groups.Count - 1)
                    group += ".";
            }
            
            return group.ToLower();
        }
    }
}